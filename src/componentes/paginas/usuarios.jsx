import  React ,{Component} from 'react'
import CartaUsuarios from '../moleculas/CartaUsuarios'
import axios from 'axios'
import GrillaUsuarios from '../organismos/GrillaUsuarios'

class Usuarios extends Component {

    constructor(props){
        super(props)

        this.state={
            users:[]
        }

    }
    
    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then(respuesta =>{
            this.setState({
                users: respuesta.data
            })
        } )
    }



    render(){
        const {users}=this.state
        return <GrillaUsuarios users={users} />
    }
}


export default Usuarios