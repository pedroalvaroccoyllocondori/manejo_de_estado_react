import React from 'react'
import { connect } from 'react-redux'
import GrillaCursos from '../organismos/GrillaCursos'



const Cursos = ( {cursos} )=><GrillaCursos cursos={cursos} />


const mapStateToProps= state=>({
    cursos:state.cursosReducer.cursos
})

export default connect(mapStateToProps,{})(Cursos)