import React , {useState,useEffect} from 'react'
import axios from "axios"
import usarCurso from '../CustomHooks/usarCurso'


const Curso =({match}) =>{

    const[comentario,establecerComentario]=useState("Sin comentario")
    const curso = usarCurso(match.params.id) 

   const MyComentario= e =>{
       establecerComentario(e.target.value)
   }

    return(
            <div className="ed-grid m-grid-3">
                {
                    curso ? (
                        <div className="ed-grid">
                        <div className="l-block ">
                        <h1 className="m-cols-3">Curso:{curso.titulo}</h1>
                        <img className="m-col-1" src={curso.imagen} alt={curso.titulo}/>
                        <p className="m-cols-2">descripcion {curso.profesor}</p>
                        </div>
                        <div className="ed-grid">
                            <h2>escribe tu comentario</h2>
                            <input type="text" placeholder="escribe ..." onChange={MyComentario.bind(this)}></input>
                            <p>{comentario}</p>
                        </div>
                        </div>
                    ):(
                        <h1>el curso no existe</h1>
                    )
                }   
            </div>
        )

}

export default Curso