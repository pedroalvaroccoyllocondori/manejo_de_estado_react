//import React from 'react'

import React,{Component} from 'react'



//class Formulario extends React.Component{
class Formulario extends Component{

    constructor(props){
        super(props)
        this.state={
            nombre:"",
            correo:"",
            fecha: new Date()
        }
        this.cambiarNombre=this.cambiarNombre.bind(this)//enlazar el cambiar nombre con el this
        this.cambiarCorreo=this.cambiarCorreo.bind(this)
        this.cambiarFecha=this.cambiarFecha.bind(this)
    }
    cambiarNombre(e){
        this.setState(
            {nombre:e.target.value}
        )
    }
    cambiarCorreo(e){
        this.setState(
            {correo:e.target.value}
        )
    }
    cambiarFecha(){
        this.setState({
            fecha:new Date()
        })
    }
    render(){
        

        return(
            <div className="ed-grid">
            <h1> formulario {this.props.nombre}</h1>
            <h4>hora actual {`${this.state.fecha.getHours()}:${this.state.fecha.getMinutes()}:${this.state.fecha.getSeconds()}`}</h4>
            <form id="elemento"> 
                <div className="form__item">
                    <label>nombre completo</label>
                    <input
                        type="text" 
                        onChange={this.cambiarNombre} 
                    />
                </div>
                <div className="form__item">
                    <label>correo electronico</label>
                    <input
                     type="email" 
                     onChange={this.cambiarCorreo } 
                     />
                </div>
                <div className="form__item">
                    <input className="button full" type="submit"value="enviarr"/>

                </div>
            </form>
            <div>
                <h2>{`hola ${this.state.nombre}`}</h2>
                <span>{`tu correo es ${this.state.correo}`}</span>
            </div>
            </div>

        )
    }



    componentDidMount(){
        let elemento= document.getElementById("elemento")
        console.log(elemento)
        this.intervalofecha=setInterval(()=>{
            this.cambiarFecha()
            console.log(new Date())
        },1000)
    }
    componentDidUpdate(prevProps,prevState){
     //   console.log(prevProps)
     //   console.log(prevState)
     //   console.log("--------")

    }
//desmontaje de componente
    componentWillUnmount(){
        clearInterval(this.intervalofecha)
    }
}



export default Formulario