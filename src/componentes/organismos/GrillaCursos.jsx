import React from 'react'
import CartaCurso from '../moleculas/CartaCurso'


const GrillaCursos=({cursos})=>(
    <div className="ed-grid m-grid-3">
        {
         cursos.map(c=>(
            <CartaCurso  
            key={c.id}
            id={c.id}
            titulo={c.titulo}
            imagen={c.imagen}
            precio={c.precio}
            profesor={c.profesor}
            />
            ))
        }
        </div>

)

export default GrillaCursos