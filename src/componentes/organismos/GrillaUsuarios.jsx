import React from 'react'
import CartaUsuarios from '../moleculas/CartaUsuarios'
import ConLoader from '../HOC/ConLoader'




const GrillaUsuarios =({users})=>(

    <div className="ed-grid">
    <h1>usuarios</h1>
    <div className="ed-grid s-grid-2 m-grid-3 l-grid-4">
        {
         users.map(
            u => (
            <CartaUsuarios 
            key={u.id}  
            name={u.name} 
            username={u.username}
            email={u.email}
            />
            ))
         }       
    </div>
</div>


)

export default ConLoader("users") (GrillaUsuarios)