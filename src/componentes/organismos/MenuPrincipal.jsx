import React from 'react'
import { NavLink } from 'react-router-dom'
import ContadorCarta from '../Atomos/ContadorCarta'


const MenuPrincipal =()=> (
    <header className="main-header">
    <div className="ed-grid s-grid-5 lg-grid-4">
      <div className="s-cols-4 lg-cols-1 s-cross-center">
          <a href="/">
              <img className="main-logo" src="https://ed.team/static/images/logo.svg" alt="Logo EDteam"/>
          </a>
      </div>
      <div className="s-grid-1 lg-cols-3 s-cross-center s-main-end header-links">
        <nav className="main-menu" id="main-menu">
          <ul>
            <li><NavLink to="/" exact activeClassName="activo">inicios</NavLink></li>
            <li><NavLink to="/cursos" activeClassName="activo">cursos</NavLink></li>
            <li><NavLink to="/formulario" activeClassName="activo">formulario</NavLink></li>
            <li><NavLink to="/usuarios" activeClassName="activo">usuarios</NavLink></li>
            <ContadorCarta />
          </ul>
        </nav>
        <div className="main-menu-toggle to-l" id="main-menu-toggle"></div>
      </div>
    </div>
  </header>


)
export default MenuPrincipal