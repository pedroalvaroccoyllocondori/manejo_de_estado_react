import { useState, useEffect } from "react"
import axios from "axios";

const useCourse = id => {

    const [course, establecerCurso] = useState({})

    useEffect(() => {
        axios.get(`https://my-json-server.typicode.com/pedroalvaroccoyllocondori/json-db/cursos/${id}`)
        .then(resp => establecerCurso(resp.data))
      }, [])

    return course  
}

export default useCourse