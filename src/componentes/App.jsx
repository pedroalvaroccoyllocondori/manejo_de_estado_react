import React from 'react';
import { Provider } from 'react-redux';
import "../styles/styles.scss";
import AplicacionRutas from './AplicacionRutas';
//import Banner from "../Banner";

import almacen from "../redux/almacen"
import { obtenerListaCursos } from '../redux/actionCreator';

almacen.dispatch(obtenerListaCursos())

const App = ()=>(
<Provider store={almacen}>
  <AplicacionRutas/>
</Provider>
)

export default App;
