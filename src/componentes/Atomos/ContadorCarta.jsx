import React from "react"
import { connect } from "react-redux"

const ContadorCarta=({tamanoCarta})=>(
    <li>
        <button className="button tiny ghost">{`carrito: ${tamanoCarta.length}`}</button>
    </li>
)


// mapear el estado global y pasarle  por props
const mapStateToProps=state=>(
    {
        tamanoCarta: state.carritoReducer.carrito
    }
)
// 
const mapDispatchToProps=()=>{

}



export default connect(mapStateToProps,mapDispatchToProps)(ContadorCarta)