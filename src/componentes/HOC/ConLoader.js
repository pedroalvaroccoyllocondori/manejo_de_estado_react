import React,{Component} from "react"


const ConLoader=(propValue)=>(WrappedComponent)=>{
    return class ConLoader extends Component{
        constructor(props){
            super(props)
        }
        render(){
            console.log(this.props)
            return this.props[propValue].length==0
            ?<h1>cargando .....</h1>
            :<WrappedComponent {...this.props} />
        }
    }
}

export default ConLoader