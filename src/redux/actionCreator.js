import Axios from "axios"
import { ADD_AL_CARRITO, DELETE_AL_CARRITO, OBTENER_LISTA_CURSOS } from "./actions"

const addAlCarrito= id =>({
    type:ADD_AL_CARRITO,
    id:id
})


const deleteDelCarrito= id =>({
    type:DELETE_AL_CARRITO,
    id:id
})

const obtenerListaCursos=()=>dispatch=>{
    Axios.get('https://my-json-server.typicode.com/pedroalvaroccoyllocondori/json-db/cursos')
    .then(response=>{
        return dispatch({
            type:OBTENER_LISTA_CURSOS,
            cursos:response.data
        })
    })
}


export {addAlCarrito ,deleteDelCarrito,obtenerListaCursos}