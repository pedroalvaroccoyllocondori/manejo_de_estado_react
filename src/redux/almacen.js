import {applyMiddleware, combineReducers, createStore} from "redux"
import { ADD_AL_CARRITO, DELETE_AL_CARRITO ,OBTENER_LISTA_CURSOS} from "./actions"
import {composeWithDevTools} from "redux-devtools-extension"
import thunk from "redux-thunk"



const inialStore={
    carrito: ["a","v"]
}

const initialCursos={
    cursos:[]
}
//empleado del almacen rootReducer
const carritoReducer=(state=inialStore,action )=>{
    if (action.type==ADD_AL_CARRITO) {

        if(state.carrito.find(a=>a===action.id))  return state
        return{
            ...state,
            carrito:state.carrito.concat(action.id)//añadimos elemetos al carrito
        }
    }

    if(action.type==DELETE_AL_CARRITO){
        return{
            ...state,
            carrito:state.carrito.filter(c=>c!==action.id)//borramos elementos del carrito
        }
    }
    console.log(action.type,action.id)
    
    return state

}

const cursosReducer=(state=initialCursos,action)=>{
   console.log(action)
    if (action.type==OBTENER_LISTA_CURSOS) {
        return{
            ...state,
            cursos:action.cursos
        }
    }

    return state
}


export default createStore(combineReducers({carritoReducer,cursosReducer}),composeWithDevTools(applyMiddleware(thunk)) )